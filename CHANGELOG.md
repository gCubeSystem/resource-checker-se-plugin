This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Resource Checker Smart Executor Plugin

## [v2.0.1]

- Removed HomeLibraryWebApp check [#23387]


## [v2.0.0]

- Contexts listing is performed via rpm-common-library [#21636]
- Ported plugin to smart-executor APIs 3.0.0 [#21618]
- Switched smart-executor JSON management to gcube-jackson [#19647]
- Creating uberjar in place of jar-with-dependecies
- Used smart-executor bom for better dependency management


## [v1.1.0] [r4.6.0] - 2017-07-25

- Added check for HomeLibraryWebApp GCoreEP


## [v1.0.0] [r4.4.0] - 2017-05-02

- First Release

