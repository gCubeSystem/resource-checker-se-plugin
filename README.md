# Resource Checker Smart Executor Plugin

Resource Checker Smart Executor Plugin periodically checks the presence of required resources in the IS V.1 in every context

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

[Resource Checker Smart Executor Plugin](https://wiki.gcube-system.org/)

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/resource-checker-se-plugin/releases).

## Authors

* **Costantino Perciante** ISTI-CNR
* **Luca Frosini** ([ORCID](https://orcid.org/0000-0003-3183-2291)) - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## How to Cite this Software

Tell people how to cite this software. 
* Cite an associated paper?
* Use a specific BibTeX entry for the software?

    @software{,
        title = {Resource Checker Smart Executor Plugin},
        author = {{Perciante, Costantino}, {Frosini, Luca}},
        organization = {ISTI - CNR},
        address = {Pisa, Italy},
        year = 2019,
        url = {http://www.gcube-system.org/}
    } 

### License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework

This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)
