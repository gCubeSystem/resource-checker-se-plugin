package org.gcube.informationsystem.resource_checker;

import java.util.HashMap;
import java.util.Map;

import org.gcube.informationsystem.resource_checker.utils.SendNotification;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.api.types.Scheduling;
import org.gcube.vremanagement.executor.client.SmartExecutorClient;
import org.gcube.vremanagement.executor.client.SmartExecutorClientFactory;
import org.gcube.vremanagement.executor.json.SEMapper;
import org.junit.Assert;
import org.junit.Test;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestResourceChecker extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(TestResourceChecker.class);
	
	@Test
	public void launchPlugin() throws Exception{
		// ContextTest.setContextByName("/d4science.research-infrastructures.eu");
		
		ContextTest.setContextByName("/gcube");
		
		ResourceCheckerPlugin checker = new ResourceCheckerPlugin();
		Map<String, Object> inputs = new HashMap<String, Object>();
		inputs.put(SendNotification.RECIPIENT_KEY, null);
		inputs.put(ResourceCheckerPlugin.ROLE_TO_NOTIFY, "Administrator");
		checker.launch(inputs);

	}

	@Test
	public void launchRemotely() throws Exception{
		ContextTest.setContextByName("/d4science.research-infrastructures.eu");
		
		String name = new ResourceCheckerPlugin().getName();
		
		SmartExecutorClient smartExecutor = SmartExecutorClientFactory.getClient(name);
		Assert.assertNotNull(smartExecutor);

		try {
			
			Map<String, Object> inputs = new HashMap<String, Object>();
			inputs.put(SendNotification.RECIPIENT_KEY, null);
			inputs.put(ResourceCheckerPlugin.ROLE_TO_NOTIFY, "Administrator");
			
			LaunchParameter launchParameter = new LaunchParameter(name, inputs);
			
			// Every day at 8:00 
			CronExpression cronExpression = new CronExpression("0 0 8 1/1 * ? *");
			Scheduling scheduling = new Scheduling(cronExpression, true);
			scheduling.setGlobal(false);
			
			launchParameter.setScheduling(scheduling);
			
			String uuidString = smartExecutor.launch(SEMapper.getInstance().marshal(launchParameter));
			logger.debug("Launched with UUID : {}", uuidString);
		} catch (Exception e) {
			logger.error("Error while launching {}", e);
			throw e;
		}

	}
}
